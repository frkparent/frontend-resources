#FRONT-END DEVELOPMENT USEFUL RESOURCES

###Web Developers worth following on Twitter:
[http://uptodate.frontendrescue.org/#follow-cool-people](http://uptodate.frontendrescue.org/#follow-cool-people)


###Online project hosting & portfolio
* [https://github.com](https://github.com)
* [https://bitbucket.org](https://bitbucket.org)
* [https://www.behance.net](https://www.behance.net)
* [http://rogerdudler.github.io/git-guide](http://rogerdudler.github.io/git-guide): You probably already use a git/svn client to do all your VCS stuff. However knowing the basics and command line cannot harm


###Experimental code and playground

Good to find inspiration for CSS transitions/animations

* [http://codepen.io](http://codepen.io)
* [http://tympanus.net/codrops/category/playground/](http://tympanus.net/codrops/category/playground/)


###HTML5 (articles and documentation)
* [http://www.html5rocks.com](http://www.html5rocks.com)
* [http://html5doctor.com](http://html5doctor.com): Good to make sure the proper selectors are use in a semantic way
* [http://html5please.com](http://html5please.com/)
* [https://html5boilerplate.com](https://html5boilerplate.com)
* [https://www.w3.org/TR/wai-aria/roles](https://www.w3.org/TR/wai-aria/roles): Know the ARIA roles and use them correctly
* [http://nicolasgallagher.com/about-html-semantics-front-end-architecture/](http://nicolasgallagher.com/about-html-semantics-front-end-architecture/): Old article but style really relevant
* [A technical specification for Web developers](https://developers.whatwg.org/)

Finally to check the browser compatibility of features I usually check [caniuse.com](http://caniuse.com)

###WordPress
* [Complete Checklist](https://capsicummediaworks.com/killer-wordpress-checklist/): Good to make sure you don't forget something before a release
* [Roots (now Sage) Theme Wrapper explained](https://groups.google.com/forum/#!topic/roots-theme/bptVrop0ONo)
* **[My personall WordPress boilerplate](https://bitbucket.org/redsix/r6stax/overview)**: Feel free to clone and use as you will. Contributions and feedback are also welcomed!
* [WordPress dashicons](https://developer.wordpress.org/resource/dashicons/): Usefull when creating new Custom Post Types


###CSS
* [http://www.w3.org/TR/css3-selectors/](http://www.w3.org/TR/css3-selectors/): list of CSS3 selectors
* [Naming and using id's and classes properly](https://2002-2012.mattwilcox.net/archive/entry/id/1058/)
* [CSS easings for transition-timing-function](https://matthewlein.com/ceaser/)
* [Font Awesome cheatsheet](http://fontawesome.io/cheatsheet/)


###Style Guide

I think every developers should have their own style guide (unless the company they work for already has one, in which case the devs should follow it). It helps develop in a consistent way making it easier for contributors and avoid surprises.

Here is a list of various style guides from different companies (Github, Google, etc.):

* [Style Guides](https://css-tricks.com/css-style-guides/)


###JavaScript

* [Mozilla Developer Network](https://developer.mozilla.org/en-US/): Go-to for documentation on every Web features
* [Basics of JS](https://github.com/raganwald/homoiconic/blob/master/2012/01/captain-obvious-on-javascript.md#readme)
* [CodeAcademy learning center](https://www.codecademy.com/learn/javascript)


###Performance:

Chrome extension to test page speed and optimization

* [https://developers.google.com/speed/pagespeed/insights/](https://developers.google.com/speed/pagespeed/insights/)
* [http://yslow.org/](http://yslow.org/)


####Creative & Inspiration
* [http://www.awwwards.com/](http://www.awwwards.com/)
* [http://onepagelove.com/](http://onepagelove.com/)
* [http://www.cssdesignawards.com/](http://www.cssdesignawards.com/)
* [http://www.cssawards.net/](http://www.cssawards.net/)
* [http://www.thebestdesigns.com/](http://www.thebestdesigns.com/)
* [http://www.designlicks.com/](http://www.designlicks.com/)


###Newsletter worth subscribing for Front-End stuff
* [http://frontendfocus.co/](http://frontendfocus.co/)
* [http://web-design-weekly.com/](http://web-design-weekly.com/)
* [http://javascriptweekly.com/](http://javascriptweekly.com/)
* [https://fivejs.codeschool.com/](https://fivejs.codeschool.com/)
* [http://sidebar.io/](http://sidebar.io/)

###Usefull tools
* [http://realfavicongenerator.net/](http://realfavicongenerator.net/): tool I use to generate cross-browser/plateforms favicons
* [https://en.wikipedia.org/wiki/Web_banner](https://en.wikipedia.org/wiki/Web_banner): Sometimes naming containers can be one of the hardest job in the world. This wiki has a lot of terms and names used in printing and in marketing. Can be usefull for naming conventions
* [http://devtoolsecrets.com/](http://devtoolsecrets.com/): Learning the in/out of the Dev Tools can help you develop faster


###Various Blog (Business, SEO, UX, HTML, CSS, JavaScript, Mobile, etc.)
* [http://www.echojs.com/](http://www.echojs.com/)
* [http://csswizardry.com/](http://csswizardry.com/)
* [http://www.smashingmagazine.com/](http://www.smashingmagazine.com/)
* [http://net.tutsplus.com/](http://net.tutsplus.com/)
* [http://www.hongkiat.com/blog/](http://www.hongkiat.com/blog/)
* [http://css-tricks.com/](http://css-tricks.com/)
* [http://alistapart.com/](http://alistapart.com/)
* [http://www.lukew.com](http://www.lukew.com/): my UX guru
* [http://www.noupe.com/](http://www.noupe.com/)
* [http://sixrevisions.com](http://sixrevisions.com/) (Business, UX, SEO)
* [http://blog.teamtreehouse.com/](http://blog.teamtreehouse.com/)

###Blogs about design & inspiration
* [http://abduzeedo.com/](http://abduzeedo.com/)
* [http://designspiration.net/](http://designspiration.net/)
* [http://logopond.com/](http://logopond.com/)
* [http://dribbble.com/](http://dribbble.com/)
* [http://pinterest.com/](http://pinterest.com/)
* [http://www.deviantart.com/](http://www.deviantart.com/)
* [http://designshack.net](http://designshack.net)
* [http://www.colourlovers.com/](http://www.colourlovers.com/): Great to find colour palettes 

###Finally a good curated list (like this one) for all Front-End resources: [http://uptodate.frontendrescue.org](http://uptodate.frontendrescue.org)

--- 

###Not related: Mac OSX apps

List of a few apps I use to make my life much easier

* [iTerm2](https://www.iterm2.com/)
* [Sequel Pro](https://www.sequelpro.com/): DB management
* [MAMP Pro](https://www.mamp.info/en/mamp-pro/): Local servers
* [SourceTree](https://www.sourcetreeapp.com/): Git client that integrates nicely with Bitbucket/Jira
* [Transmit](https://panic.com/transmit/): FTP Transfer App
* [Sublime Text](https://www.sublimetext.com/): No I don't use VIM. No I won't use it so stop dude.
* [Bartender](https://www.macbartender.com/): Keep that top bar clean. Useful for OCD people like me
* [Airmail](http://airmailapp.com/): Mail Client
* [Alfred](https://www.alfredapp.com/): Spotlight replacement that adds hotkeys & keywords
* [CloudApp](https://www.getcloudapp.com/): Upload & share screenshot quickly
* [MacDown](http://macdown.uranusjr.com/): Markdown Editor